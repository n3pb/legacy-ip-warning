# Legacy IP Warning

Future home of the history of the Legacy IP Only Warning stickers.

Dropbox

![legacy IP only caution sticker](https://www.dropbox.com/s/th4jlf04ek4h4o9/legacy-caution.png)

Dropbox 2

<img src=https://www.dropbox.com/s/th4jlf04ek4h4o9/legacy-caution.png>

Drive

![legacy IP only caution sticker](https://drive.google.com/file/d/12k2rHvIyjA-DXSiPgWY67TM_0NdmDXSN/view?usp=share_link)

Drive 2

<img src="https://drive.google.com/file/d/12k2rHvIyjA-DXSiPgWY67TM_0NdmDXSN/view?usp=share_link">

HTTP HTML

<img src="https://ntpstats.n3pb.org/m-7-loop-s.png">

HTTP Markdown

![legacy IP only caution sticker](https://ntpstats.n3pb.org/m-7-loop-s.png)

Iframe:

<figure class="video_container">
  <iframe src="https://photos.app.goo.gl/ADH7WVDMB6617xia9" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Flickr:
![legacy ip sticker](https://live.staticflickr.com/7283/8748637920_f8cf64856f_c_d.jpg)
